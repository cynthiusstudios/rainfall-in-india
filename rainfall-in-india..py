# We Are Observing The Data Collected Of Rainfall In India Between 1901 - 2015

# Importing Libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

# Importing Dataset
data = pd.read_csv("rainfall.csv")

# Data Cleaning
# Information Of The Data
data.info()
# Calculating Null Values
data.isnull().sum()
# Dropping Rows With NulL Values
data = data.dropna(axis = 0)

# Creating Graphs To See Which Top 10 States Saw The Highest Annual Rainfalls Throughout The Century
y = data.groupby("SUBDIVISION")["ANNUAL"].agg('sum').sort_values(ascending = False)[0:10].index
x = data.groupby("SUBDIVISION")["ANNUAL"].agg('sum').sort_values(ascending = False)[0:10]
sns.barplot(x = x, y = y)
plt.ylabel("States/Union Territories")
plt.xlabel("Total Annual Rainfall In The Century")
plt.title("Top States / UT Which Recieved Maximum Rainfall In The Century")

# Top 5 Years Which Saw The Highest Rainfall In Every Region
list_of_areas = data.SUBDIVISION.value_counts()
plt.figure(figsize = (30,70))
j = 0
for i in list_of_areas.index:
    j+=1
    plt.subplot(12,3,j)
    highest_rainfall = data[data.SUBDIVISION == i].sort_values(by = "ANNUAL", ascending = False)[0:5]
    x = highest_rainfall.YEAR.sort_values(ascending = False)
    y = highest_rainfall.ANNUAL
    sns.barplot(x=x, y=y)
    plt.ylabel("Annual Rainfall")
    plt.xlabel("Year")
    plt.title("Top 5 Years That Saw Highest Rainfall in "+i)
    